import { getElemTop } from 'react-router-scroll-position';
import animateScrollTo from 'animate-scroll-to';

/**
 * Handle hash locations
 */
export default class Links {
  /**
   * Construct new object from form element
   */
  constructor(elem, history) {
    // This binding is necessary to make `this` work in the callback
    this.boundNavigate = (e) => this.navigate(e);
    this.history = history;

    /**
     * Add click listener to all links
     */
    const excludedLinks = Array.from(elem.querySelectorAll('div[data-nolinks] a'));
    const allLinks = Array.from(elem.getElementsByTagName('a'));
    this.links = allLinks.filter((htmlElement) => {
      const includes = excludedLinks.includes(htmlElement);
      return !includes;
    });
    this.length = this.links.length;
    this.addListeners();
  }

  /**
   * Loop over all links to bind scroll function
   *
   * @return void
   */
  addListeners() {
    let i = 0;

    for (i; i < this.length; i += 1) {
      const link = this.links[i];

      if (link.hasAttribute('href')) {
        const href = link.getAttribute('href');

        if (href.length > 0) {
          const isHash = href[0] === '#';

          if (isHash) {
            if (href.length > 1) {
              link.addEventListener('click', Links.toHash, false);
            } else {
              link.addEventListener('click', Links.preventDefault, false);
            }
          } else if (!link.hasAttribute('target')) {
            link.addEventListener('click', this.boundNavigate, false);
          }
        }
      }
    }
  }

  /**
   * Loop over all links to remove scroll function
   *
   * @return void
   */
  removeListeners() {
    let i = 0;

    for (i; i < this.length; i += 1) {
      const link = this.links[i];

      if (link.hasAttribute('href')) {
        const href = link.getAttribute('href');

        if (href.length > 0) {
          const isHash = href[0] === '#';

          if (isHash) {
            if (href.length > 1) {
              link.removeEventListener('click', Links.toHash, false);
            } else {
              link.removeEventListener('click', Links.preventDefault, false);
            }
          } else if (!link.hasAttribute('target')) {
            link.removeEventListener('click', this.boundNavigate, false);
          }
        }
      }
    }
  }

  /**
   * Navigate to new page
   *
   * @param e object Mouse event that's triggered after link click
   *
   * @return void
   */
  navigate(e) {
    e.preventDefault();

    // Get href from elem
    const href = e.currentTarget.getAttribute('href');

    // New tab check
    const hasProtocol = href.includes('http://') || href.includes('https://');
    const isOrigin = href.includes(window.location.origin);

    // Current window check
    const isMail = href.includes('mailto:');
    const isTel = href.includes('tel:');

    // Check what to do with link
    if (hasProtocol && !isOrigin) {
      window.open(href);
    } else if (isMail || isTel) {
      window.location.href = href;
    } else {
      this.history.push(href.replace(window.location.origin, ''));
    }
  }

  /**
   * Prevent normal behaviour
   *
   * @param e object Mouse event that's triggered after link click
   *
   * @return void
   */
  static preventDefault(e) {
    e.preventDefault();
  }

  /**
   * Scroll to hash location from a link
   *
   * @param e object Mouse event that's triggered after link click
   *
   * @return void
   */
  static toHash(e) {
    e.preventDefault();

    const target = document.querySelector(e.currentTarget.getAttribute('href'));
    const top = getElemTop(target);

    animateScrollTo(top, 500);
  }
}
