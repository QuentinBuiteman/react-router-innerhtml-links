# React Router innerHTML Links
Used to let innerHTML links be handled the react-router way. For example links created in WordPress' wysiwyg editor.

## Installation
Using NPM:

```
npm install react-router-innerhtml-links --save
```

Using Yarn:

```
yarn add react-router-innerhtml-links
```

## How to use

### Import
Import the class in the component that needs it.

```js
import Links from 'react-router-innerhtml-links';
```

### Mount
Run it on mount. Bind it to the component so that you can run it on Unmount again to remove listeners.

Accepts two parameters

* An element ref ([docs](https://reactjs.org/docs/refs-and-the-dom.html#adding-a-ref-to-a-dom-element)).
* [Browser history](https://www.npmjs.com/package/history#usage).

```js
componentDidMount() {
  this.links = new Links(this.div, storeHistory);
}
```

### Unmount
Remove event listeners.

```js
componentWillUnmount() {
  this.links.removeListeners();
}
```

### Exclude
You can exclude links in elements by adding a `data-nolinks` attribute to the parent.

```html
<div data-nolinks>
  <a href="#">Excluded link</a>
</div>
```
